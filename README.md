# Noteworthy-Child

This is a child theme for the awesome minimal [Noteworthy](https://github.com/kimcc/hugo-theme-noteworthy) theme - the "minimalist Hugo theme for writers and bloggers".

Last updated when the parent theme was on commit:   `de668f23496e2658924ea749d87e7acbd6fad405`
